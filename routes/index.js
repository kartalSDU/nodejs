var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');

/* GET home page. */
router.get('/', function(req, res, next) {
	var MongoClient = mongodb.MongoClient;
	var url = 'mongodb://localhost:27017/sampsite';

	MongoClient.connect(url, function(err, db) {
		if(err) {
			console.log('Unable to connect to the server', err);
		} else {
			console.log('Connected to server');

			var collection = db.collection('books');

			collection.find({}).toArray(function(err, result) {
				if(err) {
					res.send(err);
				} else if(result.length) {
					res.render('index', {
						'bookslist' : result
					});
				} else {
					res.send('Books not found');
				}
				db.close();
			});
		}
	});
});

router.get('/newbook', function(req, res){
	res.render('newbook');
});

router.get('hello', function(req, res) {
	res.render('hello');
});

router.post('/deletebook', function(req, res) {
	res.render('removebook');
});

router.post('/addbook', function(req, res) {
	var MongoClient = mongodb.MongoClient;
	var url = 'mongodb://localhost:27017/sampsite';

	MongoClient.connect(url, function(err, db) {
		if(err) {
			console.log('Unable to connect to the server', err);
		} else {
			console.log('Connected to server');

			var collection = db.collection('books');
			var book = {name: req.body.name, author: req.body.author};

			collection.insert([book], function(err, result) {
				if(err) {
					console.log(err);
				} else {
					res.redirect('/');
				}
				db.close();
			});
		}
	});
});

module.exports = router;